import React, { Component } from 'react';
import Description from './Description.js'
import './App.css';
import NotAvailableImg from './assets/na.jpg'

class Search extends Component {
    constructor(props) {
        super(props);
        this.loadMore = this.loadMore.bind(this);
        this.load = this.load.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.search = this.search.bind(this);
        this.returnSearch = this.returnSearch.bind(this);
        this.returnThumbnail = this.returnThumbnail.bind(this);
        this.returnAuthors = this.returnAuthors.bind(this);
        this.returnPublisherData = this.returnPublisherData.bind(this);
        this.state = {
            indexStart: 0,
            indexEnd: 5,
            searchQuery: '',
            searchResults: undefined,
            loadMoreText: "Load More"
        }
    }

    handleSearch(e) {
        this.setState({ searchQuery: e.target.value });
    }

    search(e) {
        e.preventDefault();
        if (this.state.searchQuery === '') {
            return;
        }
        else {
            fetch('https://www.googleapis.com/books/v1/volumes?q=' + this.state.searchQuery + "&startIndex=0&maxResults=40&key=AIzaSyBRncw-D-Jgk8Q_oxUWbTfHiBKk-K_lqdk")
                .then(res => res.json())
                .then(body => {
                    this.setState({ searchQuery: '', searchResults: body.items, indexStart: 0, indexEnd: 5 })
                })
        }
    }

    returnSearch() {
        return (
            <div>
                <h1 className='heading'>Simple Book Search</h1>
                <form className='form' onSubmit={this.search}>
                    <input className='input' type='text' placeholder='Enter Book Title' onChange={this.handleSearch} value={this.state.searchQuery}></input>
                    <input className='submit' type='submit' value='Search'></input>
                </form>
            </div>
        );
    }

    load(e) {
        e.preventDefault();
        if (this.state.indexStart === 30) {
            this.setState({ indexStart: this.state.indexStart + 5, indexEnd: this.state.indexEnd + 5, loadMoreText: "No More Results" });
        }
        else this.setState({ indexStart: this.state.indexStart + 5, indexEnd: this.state.indexEnd + 5 });
    }


    loadMore(fullArray, indexStart, indexEnd) {
        let pageArray = [];
        pageArray.length = 0;
        pageArray = fullArray.slice(indexStart, indexEnd);
        return pageArray;
    }

    returnRating(x) {
        if (x.volumeInfo.averageRating === undefined) {
            return (<div className='info-rating'>Not yet rated</div>)
        }
        else {
            return (<div className='info-rating'>{x.volumeInfo.averageRating}/5</div>);
        }
    }

    returnPrice(x) {
        if (x.saleInfo.saleability === 'NOT_FOR_SALE') {
            return (<div className='info-price'>Not for sale</div>)
        }
        else if(x.saleInfo.saleability === 'FREE'){
            return (<div className='info-price'>Free</div>)
        }
        else {
            return (<div className='info-price'>${x.saleInfo.listPrice.amount}</div>)
        }
    }

    returnDesription(x) {
        return (<Description description={x.volumeInfo.description}></Description>)
    }

    returnThumbnail(x) {
        if (x.volumeInfo.imageLinks === undefined) {
            return (<img className='image' alt='Book Cover' src={NotAvailableImg}></img>);
        }
        else return (<img className='image' alt='Book Cover' src={x.volumeInfo.imageLinks.thumbnail}></img>);
    }

    returnAuthors(x) {
        if (x.volumeInfo.authors === undefined) {
            return (<div className='info-authors'>No Authors Available - {this.returnPublisherData(x)}</div>)
        }
        else return (<div className='info-authors'>{x.volumeInfo.authors.map((x, i) => <div key={i}>{x}</div>)} - {this.returnPublisherData(x)}</div>);
    }

    returnPublisherData(x){
        if(x.volumeInfo.publisher === undefined || x.volumeInfo.publishedDate === undefined){
            return (<div className='info-pub'>No publisher info available</div>)
        }
        else {
            return (<div className='info-pub'>{x.volumeInfo.publisher}, {x.volumeInfo.publishedDate}</div>)
        }
    }

    render() {
        if (this.state.searchResults === undefined) {
            return (this.returnSearch())
        }
        else {
            return (
                <div className="App">
                    {this.returnSearch()}
                    <div>
                        {this.loadMore(this.state.searchResults, this.stateindexStart, this.state.indexEnd).map((x, i) =>
                            <div className='book-card' key={i}>
                                <div className='book-cover-img'>
                                    {this.returnThumbnail(x)}
                                </div>
                                <div className='info'>
                                    <div className='info-title'>{x.volumeInfo.title}</div>
                                    {this.returnAuthors(x)}
                                    <div className='info-cat'>{x.volumeInfo.categories}</div>
                                    {this.returnRating(x)}
                                    {this.returnPrice(x)}
                                    {this.returnDesription(x)}
                                </div>
                            </div>
                        )}
                        <div>
                            <button className='load-more' onClick={this.load}>{this.state.loadMoreText}</button>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default Search;
